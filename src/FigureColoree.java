import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Shape;

public abstract class FigureColoree{
	private static final int TAILLE_CARRE_SELECTION = 100;
	private boolean selected;
	private Color couleur;
	private int nbClics;
	private int nbPoints;
	private Polygon polygon;

	private Point[] tabPoints;


	public FigureColoree(int nbP, int nbC, Color c) {
		this.selected = false;
		this.couleur = c;
		this.tabPoints = new Point[nbP];
		this.nbClics = nbC;
		this.nbPoints = nbP;
	}

	public abstract int nbPoint();

	public abstract int nbClics();

	public void modifierPoint(Point[] tabP) {
		for (int i = 0; i < nbClics; i++) {
			tabPoints[i] = tabP[i];
		}
	}

	public void translation(int dX,int dY) {

		for (int i = 0; i < getTabPoints().length; i++) {
			getTabPoints()[i].translation(dX, dY);
		}
	}

	public abstract void afficher(Graphics e);

	public abstract boolean coinSelect(int x, int y);

	public void selectionne() {
		this.selected = true;
	}
	
	public boolean coinSelctionne(int xSouris, int ySouris) {
		
		for (int i = 0; i < tabPoints.length; i++) {
			if (tabPoints[i].dansPoint(xSouris, ySouris)) {
				return true;
			}
		}
		
		return false;
	}
	
	public Point getCoinSelectionne() {
		for (int i = 0; i < tabPoints.length; i++) {
			if (tabPoints[i].getSelected()) {
				return tabPoints[i];
			}
		}
		return null;
	}
	public void deSelectionne() {
		this.selected = false;
	}


	public void changeCouleur(Color c) {
		this.couleur = c;
	}

	public int getNbPoints() {
		return nbPoints;
	}

	public Point[] getTabPoints() {
		return tabPoints;
	}

	public Color getCouleur() {
		return this.couleur;
	}

	public boolean getSelected() {
		return this.selected;
	}


	public String toString(){
		String res = "";
		res = "Forme : \n";

		for (int i = 0;i < this.tabPoints.length; i++) {
			res += "\t" + this.tabPoints[i].getX() + " ," +  this.tabPoints[i].getY();
		}
		return res;
	}



	public abstract FigureColoree getType(Color c);

	public abstract Polygon getPolygon();

}
