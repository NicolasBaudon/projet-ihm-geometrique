import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;


public abstract class Polygone extends FigureColoree{
	private Polygon polygon;

	public Polygone(int nbP, int nbC,Color c) {
		super(nbP, nbC,c);
	}


	public abstract int nbClics();

	public void afficher(Graphics e) {
		int[] tabX = new int[nbPoint()];
		int[] tabY = new int[nbPoint()];

		for (int i = 0; i < nbPoint(); i++) {
			tabX[i] = getTabPoints()[i].getX();
			tabY[i] = getTabPoints()[i].getY();
		}

		this.polygon = new Polygon(tabX,tabY,getNbPoints());

		e.setColor(getCouleur());
		e.fillPolygon(this.polygon);

		System.out.println(getSelected());


		for (int i = 0; i < getTabPoints().length; i++) {
			getTabPoints()[i].afficher(e, getCouleur());
		}


	}

	public boolean coinSelect(int xSouris,int ySouris) {
		boolean xBon = xSouris < (getTabPoints()[0].getX() + 20) && xSouris > (getTabPoints()[0].getX() - 20);
		boolean yBon = ySouris < (getTabPoints()[0].getY() + 20) && ySouris > (getTabPoints()[0].getY() - 20);

		if (xBon && yBon) {
			System.out.println("corner selectec");
			return true;
		}else {
			return false;
		}
	}

	@Override
	public Polygon getPolygon() {
		// TODO Auto-generated method stub
		return this.polygon;
	}


	//public abstract void modifierPoints(Point p);

}
