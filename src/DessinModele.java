import java.util.ArrayList;
import java.util.Observable;import org.w3c.dom.css.Rect;

import java.awt.Color;
import java.awt.Graphics;
//import java.awt.Point;
public class DessinModele extends Observable{

	private ArrayList<FigureColoree> lfc;
	private ArrayList<Trait> tf;

	private FigureColoree figureEnCours;
	private int nbPointsClique;
	private int nbClics;
	private Point[] pointsCliques;

	public DessinModele() {

		lfc = new ArrayList<FigureColoree>();
		tf = new ArrayList<Trait>();

	}



	public void ajouterFigure(FigureColoree fc) {
		this.lfc.add(fc);
		setChanged();
		notifyObservers();
	}
	
	public void ajouterTrait(Trait t) {
		this.tf.add(t);
		setChanged();
		notifyObservers();
	}

	public void afficher(Graphics e) {
		for (int i = 0; i < this.lfc.size(); i++) {
			this.lfc.get(i).afficher(e);
		}
		
		for (Trait trait : tf) {
			trait.afficher(e);
		}
	}

	public Point[] getPointsCliques() {
		return this.pointsCliques;
	}

	public ArrayList<FigureColoree> getLfc() {
		return this.lfc;
	}

	boolean dansListe(int x,int y) {
		for (FigureColoree figure : this.lfc) {
			if (figure.getPolygon().contains(new java.awt.Point(x,y))) {
				figure.selectionne();
				actualiser();
				return true;
			}
		}

		return false;
	}

	public FigureColoree getSelected() {

		for (FigureColoree figure : this.lfc) {
			if (figure.getSelected()) {
				return figure;
			}
		}
		return null;
	}

	public boolean dansCoin(int xSouris, int ySouris ) {
		for(FigureColoree figures : this.lfc) {
			if (figures.coinSelctionne(xSouris, ySouris)) {
				return true;
			}
		}

		return false;
	}

	public Point getCoinSelectionnne() {
		for(FigureColoree figures : this.lfc) {
			if (figures.getCoinSelectionne() != null) {
				return figures.getCoinSelectionne();
			}
		}
		return null;
	}

	public void changerCouleur(Color c) {
		getSelected().changeCouleur(c);
	}

	

	public void setColor(Color c) {
		figureEnCours.changeCouleur(c);
	}

	public void actualiser() {
		setChanged();
		notifyObservers();
	}
}
