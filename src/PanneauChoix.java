import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class PanneauChoix extends JPanel{

	private VueDessin vueD;
	private FigureColoree figureC;
	private JComboBox<String> cFigures;
	private JComboBox<String> cCouleur;
	private JRadioButton btFigure;
	private JRadioButton btMainLevee;
	
	private Color couleur;
	public PanneauChoix(VueDessin vd) {
		this.couleur = Color.black;
		this.vueD = vd;


		btFigure = new JRadioButton("Nouvelle figure");
		btMainLevee = new JRadioButton("Tracé à main levée");
		JRadioButton btManip = new JRadioButton("Manipulations");

		ButtonGroup bg = new ButtonGroup();

		bg.add(btFigure);
		bg.add(btMainLevee);
		bg.add(btManip);

		btMainLevee.setSelected(true);
		vueD.tracer();//par default en mode trace de trait			

		this.add(btFigure);
		this.add(btMainLevee);
		this.add(btManip);
		this.setLayout(new GridLayout(2,1));

		cCouleur = new JComboBox<String> (new String[] {"noir","rouge", "bleu", "vert"});
		cFigures = new JComboBox<String> (new String[] {"rectangle", "triangle", "quadrilatere"});



		this.add(cCouleur);
		this.add(cFigures);

		cFigures.setEnabled(false);
		
		EcouteurMenu ecouteur = new EcouteurMenu();
		btFigure.addActionListener(ecouteur);
		this.cFigures.addActionListener(ecouteur);
		cCouleur.addActionListener(ecouteur);

		btFigure.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (btFigure.isSelected()) {
					choixFigure();
					cCouleur.setEnabled(true);
					cFigures.setEnabled(true);
					figureC = new Rectangle(couleur);

				}
			}
		});

		btMainLevee.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (btMainLevee.isSelected()) {
					System.out.println("traecerrrr");
					cCouleur.setEnabled(true);
					cFigures.setEnabled(false);
					vueD.tracer();				
				}
			}
		});

		btManip.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (btManip.isSelected()) {
					cFigures.setEnabled(false);
					cCouleur.setEnabled(false);
					vueD.manipuler();					
				}

			}
		});




		

	}


	public class EcouteurMenu implements ActionListener{
		public void actionPerformed(ActionEvent e){

			Object source = e.getSource();
			
			if (btFigure.isSelected()) {
				choixFigure();
			}
				
				choixCouleur();

			
		}
	}


	private void choixFigure() {
		switch(cFigures.getSelectedIndex()){

		case 0:
			System.out.println("rectangle");
			this.vueD.supListener();
			figureC = new Rectangle(couleur);
			break;
		case 1:
			System.out.println("triangle");
			this.vueD.supListener();
			figureC = new Triangle(couleur);

			break;
		case 2:
			System.out.println("quadrilatere");
			this.vueD.supListener();
			figureC = new Quadrilatere(couleur);

			break;
		default:

		}
		vueD.construir(figureC);

	}

	private void choixCouleur() {
		Color c = null;
		switch(cCouleur.getSelectedIndex()){
		case 0:
			c = Color.black;
			break;
		case 1:
			c = Color.red;
			break;
		case 2:
			c = Color.blue;
			break;
		case 3:
			c = Color.green;
			break;
		default:
		}
		
		if (btMainLevee.isSelected()) {
			vueD.setCouleurTrait(c);

		}else {
			vueD.setCouleurFigure(c);

		}


	}



}
