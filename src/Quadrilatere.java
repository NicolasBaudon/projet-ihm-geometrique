import java.awt.Color;
import java.awt.Graphics;

public class Quadrilatere extends Polygone{
	private Color couleur;
	private final static int NB_CLICS =  4;
	private final static int NB_POINTS =  4;


	public Quadrilatere(Color c) {
		super(NB_POINTS,NB_CLICS,c);
		this.couleur = c;
	}

	public int nbPoint() {
		return NB_POINTS;

	}
	@Override
	public int nbClics() {
		return NB_CLICS;
	}

	public FigureColoree getType(Color c) {
		Quadrilatere quadrilatere = new Quadrilatere(c);
		return quadrilatere;
	}

}
