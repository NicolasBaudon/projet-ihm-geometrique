import java.awt.Color;
import java.awt.Graphics;

public class Triangle extends Polygone{

	private Color color;
	private final static int NB_CLICS =  3;
	private final static int NB_POINTS =  3;

	public Triangle(Color c) {
		super(NB_POINTS,NB_CLICS,c);

		this.color = c;
	}

	public int nbPoint() {
		return NB_POINTS;

	}
	@Override
	public int nbClics() {
		return NB_CLICS ;
	}


	public boolean contient(int x,int y) {
		return true;
	}


	public FigureColoree getType(Color c) {
		Triangle triangle = new Triangle(c);
		return triangle;
	}


}
