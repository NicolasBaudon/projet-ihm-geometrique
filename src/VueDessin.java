import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class VueDessin extends JPanel implements Observer{

	private DessinModele modele;
	private FabricantFigures ff;
	private ManipulateurFormes mf;
	private TraceurForme tf;

	public VueDessin(DessinModele dm) {
		this.setPreferredSize(new Dimension(500,400));
		this.modele = dm;

	}

	public DessinModele getDessin() {
		return this.modele;
	}


	@Override
	public void update(Observable arg0, Object arg1) {

		// TODO Auto-generated method stub

		modele = (DessinModele) arg0;

		repaint();

	}

	@Override
	public void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);


		try {

			modele.afficher(arg0);
			System.out.println("selected : " + modele.getSelected().getSelected());
			

		} catch ( NullPointerException e) {
			System.out.println("erreur");
		}

	}

	public void construir(FigureColoree fc) {
		supListener();

		this.ff = new FabricantFigures(modele, fc);
		this.addMouseListener(this.ff);
		//		this.addMouseMotionListener(ff);

		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
	}


	public void supListener() {
		this.removeMouseListener(ff);
		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
		this.removeMouseListener(tf);
		this.removeMouseMotionListener(tf);
		//		this.removeMouseMotionListener(ff);

	}

	public FabricantFigures getFf() {
		return ff;
	}

	public void manipuler() {
		supListener();

		this.mf = new ManipulateurFormes(this.modele);
		this.addMouseMotionListener(mf);
		this.addMouseListener(mf);


	}

	public void tracer() {
		supListener();

		this.tf = new TraceurForme(this.modele);
		this.addMouseMotionListener(tf);
		this.addMouseListener(tf);

	}


	public void setCouleurFigure(Color c) {
		this.ff.setCouleur(c);

	}
	public void setCouleurTrait(Color c) {
		this.tf.setCouleur(c);

	}
}



