import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Fenetre {

	public static void main(String[] args) {

		DessinModele dm = new DessinModele();		
		VueDessin vd = new VueDessin(dm);
		PanneauChoix pc = new PanneauChoix(vd);

		dm.addObserver(vd);

		JFrame f = new JFrame();

		f.getContentPane().setLayout(new BorderLayout());
		f.getContentPane().add(pc,BorderLayout.NORTH);
		f.getContentPane().add(vd,BorderLayout.SOUTH);

		f.pack();

		f.setPreferredSize(new Dimension(500, 500));
		f.setVisible(true);

	}
}
