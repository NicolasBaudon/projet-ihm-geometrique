import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class TraceurForme implements MouseListener, MouseMotionListener{

	private DessinModele modele;
	private int xSouris, ySouris;
	private int lastX, lastY;
	private Color couleur;

	public TraceurForme(DessinModele m) {
		this.modele = m;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("Pressed Traceur");
		this.lastX = e.getX();
		this.lastY = e.getY();

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		System.out.println(e.getX() + " ," + e.getY());
		xSouris = e.getX();
		ySouris = e.getY();

		tracer(xSouris, ySouris);
	}



	public void tracer(int xs,int ys) {


		modele.ajouterTrait(new Trait(this.lastX, this.lastY, xs, ys, this.couleur));
		this.lastX = xs;
		this.lastY = ys;
	}
	
	public void setCouleur(Color c) {
		System.out.println("traceur ; " + c);
		this.couleur = c;
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
