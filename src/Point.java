import java.awt.Color;
import java.awt.Graphics;

public class Point {
	private int x;
	private int y;
	private final static int DIAMETRE = 15;
	private final static int ZONE_SELECTION = 45;

	private boolean selected;

	public Point(int pointX, int pointY) {
		this.x = pointX;
		this.y = pointY;
		this.selected = false;
	}

	public Point(Point p) {
		this.x = p.getX();
		this.y = p.getY();
	}
	public double distance(Point p) {
		double d = Math.sqrt((Math.pow((p.getX()-this.x), 2)+Math.pow((p.getY()-this.y), 2)));
		return d;
	}

	public boolean dansPoint(int xS, int yS) {
		boolean xBon = xS > this.x - ZONE_SELECTION && xS < this.x + ZONE_SELECTION;
		boolean yBon =  yS > this.y - ZONE_SELECTION && yS < this.y + ZONE_SELECTION;
		if (xBon && yBon) {
			this.selected = true;
			return true;
		}else {
			this.selected = false;
			return false;
		}
	}

	public void afficher(Graphics e,Color c) {
		e.setColor(c);

		if (selected) {
			e.fillOval(this.x- DIAMETRE/2, this.y -DIAMETRE/2, DIAMETRE,DIAMETRE);
		}		
	}


	public void incrementerX(int iX) {
		this.x += iX;
	}

	public void incrementerY(int iY) {
		this.y += iY;
	}

	public void modifierX(int iX) {
		this.x = iX;
	}

	public void modifierY(int iY) {
		this.y = iY;
	}

	public void translation(int dX, int dY) {
		this.x += dX;
		this.y += dY;
	}

	public int getX() {
		return x;
	}

	public int getY() {

		return y;
	}

	public boolean getSelected() {
		return this.selected;
	}
}
