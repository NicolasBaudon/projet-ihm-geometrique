import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FabricantFigures implements MouseListener{

	private FigureColoree figureEnCours;
	private DessinModele dessinM;
	private int nbClics;
	private Point[] tabPoints;
	private Color couleur;

	public FabricantFigures(DessinModele dessin,FigureColoree fc) {
		this.dessinM = dessin;
		this.nbClics = 0;
		this.figureEnCours = fc;
	}

	public void ajouterFigure(FigureColoree fc){

	}

	public void mouseClicked(MouseEvent e) {
		System.out.println("clic");

		for (FigureColoree figure : this.dessinM.getLfc()) {
			figure.deSelectionne();
			System.out.println("else");
			this.dessinM.actualiser();
		}

		if (this.dessinM.dansListe(e.getX(), e.getY())) {

			

			System.out.println("Dedans");
			this.nbClics = 0;
			this.tabPoints = null;
			this.dessinM.getSelected().changeCouleur(couleur);

			this.dessinM.actualiser();

			System.out.println("selected : " + this.dessinM.getSelected().getSelected());


		}else {		
			
			if (this.nbClics == 0) {
				this.tabPoints = new Point[this.figureEnCours.nbClics()];

			}
			this.tabPoints[this.nbClics] = new Point(e.getX(), e.getY());
			this.nbClics++;

			if (this.nbClics == this.figureEnCours.nbClics()) {
				this.nbClics = 0;
				this.figureEnCours.modifierPoint(this.tabPoints);
				this.dessinM.ajouterFigure(this.figureEnCours);
			}

			this.figureEnCours = this.figureEnCours.getType(couleur);
		}



	}

	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {

		//					this.dessinM.dansListe(e.getX(), e.getY());
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


}
