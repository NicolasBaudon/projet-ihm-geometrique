import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends Polygone{
	private Color couleur;
	private final static int NB_CLICS =  2;
	private final static int NB_POINTS =  4;

	public Rectangle(Color c) {
		super(NB_POINTS,NB_CLICS,c);
		couleur = c;
	}

	public int nbPoint() {
		return NB_POINTS;

	}
	@Override
	public int nbClics() {
		return NB_CLICS ;
	}
	


		public void modifierPoint(Point[] tabP) {
			
			super.getTabPoints()[0] = new Point(tabP[0].getX(), tabP[0].getY()); 
			super.getTabPoints()[1] = new Point(tabP[1].getX(), tabP[0].getY()); 
			super.getTabPoints()[2] = new Point(tabP[1].getX(), tabP[1].getY()); 
			super.getTabPoints()[3] = new Point(tabP[0].getX(), tabP[1].getY()); 

			
		}
		
		public FigureColoree getType(Color c) {
			Rectangle rectangle = new Rectangle(c);
			return rectangle;
		}

}