import java.awt.Color;
import java.awt.Graphics;

public class Trait {
	private int xDebut,yDebut, xFin, yFin;	
	private Color couleur;
	
	public Trait(int xd,int yd, int xf,int yf, Color c) {
		this.xDebut = xd;
		this.yDebut = yd;
		this.xFin = xf;
		this.yFin = yf;
		this.couleur = c;
	}
	
	public void afficher(Graphics g) {
//		System.out.println(couleur);
		g.setColor(this.couleur);
		g.drawLine(xDebut, yDebut, xFin, yFin);

	}
	public int getXDebut() {
		return this.xDebut;
	}
	
	public int getYDebut() {
		return this.yDebut;
	}
	
	public int getXFin() {
		return this.xFin;
	}
	
	public int getYFin() {
		return this.yFin;
	}
	
	public Color getCouleur() {
		return this.couleur;
	}
}
