import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class ManipulateurFormes implements MouseMotionListener, MouseListener{

	private ArrayList<FigureColoree> flg;
	private DessinModele modele;	
	private Point pointSelection;
	private Point pointPrec;


	public ManipulateurFormes(DessinModele dm) {
		
		this.modele = dm;
	}


	@Override
	public void mouseClicked(MouseEvent e) {

	}
	@Override
	public void mousePressed(MouseEvent e) {
		for (FigureColoree figure : this.modele.getLfc()) {
			figure.deSelectionne();
			System.out.println("else");
			this.modele.actualiser();
		}

		if (this.modele.dansListe(e.getX(), e.getY())) {


//			this.pointSelection = new Point(e.getX(), e.getY());
			this.pointPrec = new Point(e.getX(),e.getY());

			System.out.println("Deans");			
			System.out.println("selected : " + this.modele.getSelected().getSelected());
		}else if (this.modele.dansCoin(e.getX(),e.getY()) && this.modele.getCoinSelectionnne() == null) {
			this.modele.actualiser();
		} 

	}


	public void mouseDragged(MouseEvent e) {


		if (this.modele.getCoinSelectionnne() != null) {
			int	distX = e.getX() - this.pointPrec.getX();
			int	distY = e.getY() - this.pointPrec.getY();
			this.modele.getCoinSelectionnne().translation(distX, distY);
			this.pointPrec = new Point(e.getX(), e.getY());

			this.modele.actualiser();

		} else if (this.modele.getSelected() != null) {
			System.out.println("drag select ok");
			int	distX = e.getX() - this.pointPrec.getX();
			int	distY = e.getY() - this.pointPrec.getY();

			this.modele.getSelected().translation(distX,distY);
			this.modele.actualiser();
			this.pointPrec = new Point(e.getX(), e.getY());
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
